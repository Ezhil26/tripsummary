import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import { TableHeader, StyledTableRow } from "../../../Base/TableHeader";
import { getComparator, sortArray } from "../../../../helpers/sorting";
import Grid from "@material-ui/core/Grid";

function createData(
  FMS_Location,
  Trip_ID,
  Store,
  Store_drops,
  distance_travelled,
  vehicle_number,
  vendor,
  contract_type,
  Actual_distance,
  Toll_charges,
  Halting_charges,
  other_charges,
  Total_other_charges
) {
  return {
    FMS_Location,
    Trip_ID,
    Store,
    Store_drops,
    distance_travelled,
    vehicle_number,
    vendor,
    contract_type,
    Actual_distance,
    Toll_charges,
    Halting_charges,
    other_charges,
    Total_other_charges,
  };
}

const rows = [
  createData(
    "Chennai DC",
    249986,
    "view_stores",
    2,
    253,
    "TN29BA6922",
    "RAJ TRANSPORT",
    "Fixed+var",
    "",
    "",
    "",
    "",
    ""
  ),
  createData(
    "Chennai DC",
    249986,
    "view_stores",
    2,
    310,
    "TN14M4795",
    "RAJ TRANSPORT",
    "Fixed+var",
    "",
    "",
    "",
    "",
    ""
  ),
  createData(
    "Chennai DC",
    257973,
    "view_stores",
    1,
    189,
    "TN22DB3831",
    "RAJ TRANSPORT",
    "Fixed+var with",
    "",
    "",
    "",
    "",
    ""
  ),
];

const headers = [
  {
    id: "FMS_Location",
    numeric: false,
    disablePadding: false,
    label: "FMS Location",
  },
  {
    id: "Trip_ID",
    numeric: true,
    disablePadding: false,
    label: "Trip ID",
  },
  {
    id: "Store",
    numeric: true,
    disablePadding: false,
    label: "Store",
  },
  {
    id: "Store_drops",
    numeric: true,
    disablePadding: false,
    label: "Store drops",
  },
  {
    id: "distance_travelled",
    numeric: true,
    disablePadding: false,
    label: "distance travelled",
  },
  {
    id: "vehicle_number",
    numeric: true,
    disablePadding: false,
    label: "vehicle number",
  },
  {
    id: "vendor",
    numeric: true,
    disablePadding: false,
    label: "vendor",
  },
  {
    id: "contract_type",
    numeric: true,
    disablePadding: false,
    label: "contract type",
  },
  {
    id: "Actual_distance",
    numeric: true,
    disablePadding: false,
    label: "Actual Distance (Km)",
  },
  {
    id: "Toll_charges",
    numeric: true,
    disablePadding: false,
    label: "Toll Charges",
  },
  {
    id: "Halting_charges",
    numeric: true,
    disablePadding: false,
    label: "Halting Charges",
  },
  {
    id: "other_charges",
    numeric: true,
    disablePadding: false,
    label: "Other Charges",
  },
  {
    id: "Total_other_charges",
    numeric: true,
    disablePadding: false,
    label: "Total Other Charges",
  },
];

const useStyles = makeStyles((theme) => ({
  fs12: {
    fontSize: "12px",
  },
  container: {
    minHeight: 475,
    maxHeight: 475,
  },
}));

export default function Costing(props) {
  const classes = useStyles();
  const { handleNext } = props;
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("FMS_Location");
  const [tableData, setTableData] = React.useState(rows);

  const handleChange = (row, event, property, index) => {
    console.log(row);
    row[property] = event.target.value;
    row["Total_other_charges"] =
      Number(row["Toll_charges"]) +
      Number(row["Halting_charges"]) +
      Number(row["other_charges"]);
    var tempTableData = [...tableData];
    tempTableData[index] = row;
    setTableData(tempTableData);
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  return (
    <Grid container spacing={2} justify="center">
      <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
        <TableContainer className={classes.container}>
          <Table stickyHeader>
            <TableHeader
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
              headers={headers}
              isCheckboxEnabled={false}
            />
            <TableBody>
              {sortArray(rows, getComparator(order, orderBy)).map(
                (row, index) => {
                  return (
                    <StyledTableRow key={index}>
                      <TableCell align="right" className={classes.fs12}>
                        {row.FMS_Location}
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        {row.Trip_ID}
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        {row.Store}
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        {row.Store_drops}
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        {row.distance_travelled}
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        {row.vehicle_number}
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        {row.vendor}
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        {row.contract_type}
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        <TextField
                          id="filled-margin-none"
                          defaultValue=""
                          className={classes.textField}
                          variant="filled"
                          fullWidth
                        />

                        {row.Actual_distance}
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        <TextField
                          id="filled-margin-none"
                          type="number"
                          onChange={(e) =>
                            handleChange(row, e, "Toll_charges", index)
                          }
                          name="Toll_charges"
                          variant="filled"
                          fullWidth
                        />
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        <TextField
                          id="filled-margin-none"
                          className={classes.textField}
                          type="number"
                          onChange={(e) =>
                            handleChange(row, e, "Halting_charges", index)
                          }
                          name="Halting_charges"
                          variant="filled"
                          fullWidth
                        />
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        <TextField
                          id="filled-margin-none"
                          type="number"
                          onChange={(e) =>
                            handleChange(row, e, "other_charges", index)
                          }
                          name="other_charges"
                          className={classes.textField}
                          variant="filled"
                          fullWidth
                        />
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        {row.Total_other_charges}
                      </TableCell>
                    </StyledTableRow>
                  );
                }
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  );
}
