import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import { TableHeader, StyledTableRow } from "../../../Base/TableHeader";
import { getComparator, sortArray } from "../../../../helpers/sorting";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import clsx from "clsx";

function createData(
    FMS_Location,
    Trip_ID,
    Store,
    Store_name,
    Vehicle_arrival_time,
    Vehicle_dispatch_time,
    POD
) {
  return {
    FMS_Location,
    Trip_ID,
    Store,
    Store_name,
    Vehicle_arrival_time,
    Vehicle_dispatch_time,
    POD
  };
}

const rows = [
    createData("Chennai DC", 249986, 1182, "Thiruvanmiyur", "", "", ""),
    createData("Chennai DC", 249986, 1185, "Thorapakkam", "", "", ""),
    createData("Chennai DC", 257973, 1169, "Gowrivakkam", "", "", ""),
];

const headers = [
    {
        id: "FMS_Location",
        numeric: false,
        disablePadding: false,
        label: "FMS Location",
      },
      {
        id: "Trip_ID",
        numeric: true,
        disablePadding: false,
        label: "Trip ID",
      },
      {
        id: "Store",
        numeric: true,
        disablePadding: false,
        label: "Store",
      },
      {
        id: "Store_name",
        numeric: false,
        disablePadding: false,
        label: "Store_name",
      },
      {
        id: "Vehicle_arrival_time",
        numeric: true,
        disablePadding: false,
        label: "Vehicle_arrival_time",
      },
      {
        id: "Vehicle_dispatch_time",
        numeric: true,
        disablePadding: false,
        label: "Vehicle_dispatch_time",
      },
      {
        id: "POD",
        numeric: false,
        disablePadding: false,
        label: "POD",
      },
  
 
];

const useStyles = makeStyles((theme) => ({
  fs12: {
    fontSize: "12px",
  },
  container: {
    minHeight: 475,
    maxHeight: 475,
  },
}));

export default function TripEnd(props) {
  const classes = useStyles();
  const { handleNext } = props;
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("FMS_Location");
  const [tableData, setTableData] = React.useState(rows);

  const test =(row,event,property,index)=>{
    console.log(row,event.target);
   row[property] = event.target.value;
   var tempTableData = [...tableData];
   tempTableData[index] = row;
   setTableData(tempTableData);
};

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  return (
    <Grid container spacing={2} justify="center">
      <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
        <TableContainer className={classes.container}>
          <Table stickyHeader>
            <TableHeader
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
              headers={headers}
              isCheckboxEnabled={false}
            />
            <TableBody>
              {sortArray(rows, getComparator(order, orderBy)).map(
                (row, index) => {
                  return (
                    <StyledTableRow key={index}>
                      <TableCell align="right" className={classes.fs12}>
                        {row.FMS_Location}
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        {row.Trip_ID}
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        {row.Store}
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        {row.Store_name}
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                      <TextField
                          id="datetime-local"
                          label="vehicle-arrival-time"
                          type="datetime-local"
                          onChange={(event)=>test(row,event,"Vehicle_arrival_time",index)}
                          className={classes.textField}
                          InputLabelProps={{
                            shrink: true,
                          }}
                        />
                        
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                      <TextField
                          id="datetime-local"
                          label="Vehicle-dispatch-time"
                          type="datetime-local"
                          onChange={(event)=>test(row,event,"Vehicle_dispatch_time",index)}
                          className={classes.textField}
                          InputLabelProps={{
                            shrink: true,
                          }}
                        />
                        
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                      <Button variant="outlined" color="secondary" onClick={(event)=>test(row,event,"POD",index)}>
                          POD
                        </Button>
                        {row.POD}
                      </TableCell>
                    </StyledTableRow>
                  );
                }
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  );
}
