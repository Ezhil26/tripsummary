import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Paper from "@material-ui/core/Paper";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
function createData(
  FMS_Location,
  Temp_Trip_ID,
  Store_details,
  total_lpn_wt_Kgs,
  total_lpn_vol_cft,
  store_drops,
  distance_travelled,
  vehicle_number,
  contract_type,
  vendor,
  vehicle_wt_capacity,
  vehicle_vol_capacity,
  vehicle_wt_utilization,
  vehicle_vol_utilization
) {
  return {
    FMS_Location,
    Temp_Trip_ID,
    Store_details,
    total_lpn_wt_Kgs,
    total_lpn_vol_cft,
    store_drops,
    distance_travelled,
    vehicle_number,
    contract_type,
    vendor,
    vehicle_wt_capacity,
    vehicle_vol_capacity,
    vehicle_wt_utilization,
    vehicle_vol_utilization,
  };
}

const rows = [
  createData(
    "Chennai DC",
    "T249986",
    "view_stores",
    1050,
    135,
    2,
    306,
    "",
    "",
    "",
    "",
    "",
    "",
    ""
  ),
  createData(
    "Chennai DC",
    "T257973",
    "view_stores",
    2560,
    105,
    2,
    279,
    "",
    "",
    "",
    "",
    "",
    "",
    ""
  ),
  createData(
    "Chennai DC",
    "T257974",
    "view_stores",
    4200,
    220,
    1,
    283,
    "",
    "",
    "",
    "",
    "",
    "",
    ""
  ),
];

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  {
    id: "FMS_Location",
    numeric: false,
    disablePadding: false,
    label: "FMS Location",
  },
  {
    id: "Temp_Trip_ID",
    numeric: false,
    disablePadding: false,
    label: "Temp Trip ID",
  },
  {
    id: "Store_details",
    numeric: false,
    disablePadding: false,
    label: "Store details",
  },
  {
    id: "total_lpn_wt_Kgs",
    numeric: true,
    disablePadding: false,
    label: "total_lpn_wt Kgs",
  },
  {
    id: "total_lpn_vol_cft",
    numeric: true,
    disablePadding: false,
    label: "total_lpn_vol_cft",
  },
  {
    id: "store_drops",
    numeric: true,
    disablePadding: false,
    label: "store_drops",
  },
  {
    id: "distance_travelled",
    numeric: true,
    disablePadding: false,
    label: "distance_travelled",
  },
  {
    id: "vehicle_number",
    numeric: false,
    disablePadding: false,
    label: "vehicle number",
  },
  {
    id: "contract_type",
    numeric: false,
    disablePadding: false,
    label: "contract_type",
  },
  {
    id: "vendor",
    numeric: false,
    disablePadding: false,
    label: "vendor",
  },
  {
    id: "vehicle_wt_capacity",
    numeric: false,
    disablePadding: false,
    label: "vehicle_wt_capacity",
  },
  {
    id: "vehicle_vol_capacity",
    numeric: false,
    disablePadding: false,
    label: "vehicle_vol_capacity",
  },
  {
    id: "vehicle_wt_utilization",
    numeric: false,
    disablePadding: false,
    label: "vehicle_wt_utilization",
  },
  {
    id: "vehicle_vol_utilization",
    numeric: false,
    disablePadding: false,
    label: "vehicle_vol_utilization",
  },
];

let API_DATA = [
  {
    vehicle_number: "TN045AC2",
    contract_type: "Fixed+var",
    vendor: "RAJ TRANSPORT",
    vehicle_wt_capacity: 2500,
    vehicle_vol_capacity: 195.84,
    vehicle_wt_utilization: "42%",
    vehicle_vol_utilization: "69%",
  },
  {
    vehicle_number: "TN14M479",
    contract_type: "Fixed+var with tax",
    vendor: "RAJ TRANSPORT",
    vehicle_wt_capacity: 2500,
    vehicle_vol_capacity: 204.04,
    vehicle_wt_utilization: "102%",
    vehicle_vol_utilization: "51%",
  },
  {
    vehicle_number: "TN22DB38",
    contract_type: "Fixed+var with tax and others",
    vendor: "RAJ TRANSPORT",
    vehicle_wt_capacity: 5500,
    vehicle_vol_capacity: 404.14,
    vehicle_wt_utilization: "76%",
    vehicle_vol_utilization: "53%",
  },
];
function EnhancedTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? "right" : "left"}
            padding={headCell.disablePadding ? "none" : "default"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(1),
  },
  table: {
    minWidth: 100,
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
}));

export default function EnhancedTableCreate() {
  const classes = useStyles();
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("Temp_Trip_ID");
  const [tableData, setTableData] = React.useState(rows);
  const [ApiData, setApiData]=React.useState("");
  const [selected, setSelected] = React.useState("");

  function handleChange(row,event,property,index) {
   row[property]=event.target.value;
   console.log(row);
   tableData[index]=row;
   setTableData(tableData);
   console.log(tableData[index]);
  }

  function handleChange1(event) {
    setSelected(event.target.value);
    let _vals = event.target.value
      ? API_DATA.filter((r) => r.contract_type === event.target.value)
      : API_DATA;
    setTableData(_vals);
  }

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {stableSort(tableData, getComparator(order, orderBy)).map(
                (row, index) => {
                  const labelId = `FMS_Location-${index}`;

                  return (
                    <TableRow hover key={row.FMS_Location}>
                      <TableCell id={labelId} scope="row" align="right">
                        {row.FMS_Location}
                      </TableCell>
                      <TableCell id={labelId} scope="row" align="right">
                        {row.Temp_Trip_ID}
                      </TableCell>
                      <TableCell align="right">{row.Store_details}</TableCell>
                      <TableCell align="right">
                        {row.total_lpn_wt_Kgs}
                      </TableCell>
                      <TableCell align="right">
                        {row.total_lpn_vol_cft}
                      </TableCell>
                      <TableCell align="right">{row.store_drops}</TableCell>
                      <TableCell align="right">
                        {row.distance_travelled}
                      </TableCell>
                      <TableCell align="right">
                        <Select
                          style={{ width: "100%" }}
                          value={row.vehicle_number}
                          onChange={(e) => handleChange(row,e,"vehicle_number",index)}
                          name="vehicle_number"
                          displayEmpty
                          className={classes.selectEmpty}
                        >
                          <MenuItem value="">select vehicle no</MenuItem>
                          <MenuItem value="TN045AC2">TN045AC2</MenuItem>
                          <MenuItem value="TN14M479">TN14M479</MenuItem>
                          <MenuItem value="TN22DB38">TN22DB38</MenuItem>
                        </Select>
                      </TableCell>
                      <TableCell align="right">
                        <Select
                          style={{ width: "100%" }}
                          value={row.contract_type}
                          onChange={handleChange}
                          name="contract_type"
                          displayEmpty
                          className={classes.selectEmpty}
                        >
                          <MenuItem value="">select contract_type</MenuItem>
                          <MenuItem value="Fixed+var">Fixed+var</MenuItem>
                          <MenuItem value="Fixed+var with tax">
                            Fixed+var with tax
                          </MenuItem>
                          <MenuItem value="Fixed+var with tax and others">
                            Fixed+var with tax and others
                          </MenuItem>
                        </Select>
                      </TableCell>

                      <TableCell align="center">{row.vendor}</TableCell>
                      <TableCell align="center">
                        {row.vehicle_wt_capacity}
                      </TableCell>
                      <TableCell align="center">
                        {row.vehicle_vol_capacity}
                      </TableCell>
                      <TableCell align="center">
                        {row.vehicle_wt_utilization}
                      </TableCell>
                      <TableCell align="center">
                        {row.vehicle_vol_utilization}
                      </TableCell>
                    </TableRow>
                  );
                }
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </div>
  );
}
