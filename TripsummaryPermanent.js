import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Paper from "@material-ui/core/Paper";
import Button from '@material-ui/core/Button';


function createData(
  FMS_Location,
  Temp_Trip_ID,
  Store_details,
  total_lpn,
  total_lpn_vol_cft,
  store_drops,
  distance_travelled,
  vehicle_number,
  vendor,
  vehicle_wt_capacity,
  vehicle_vol_capacity,
  vehicle_picture
) {
  return {
    FMS_Location,
    Temp_Trip_ID,
    Store_details,
    total_lpn,
    total_lpn_vol_cft,
    store_drops,
    distance_travelled,
    vehicle_number,
    vendor,
    vehicle_wt_capacity,
    vehicle_vol_capacity,
    vehicle_picture,
  };
}

const rows = [
  createData(
    "Chennai DC",
    249986,
    "view_stores",
    1050,
    1050,
    2,
    306,
    "TN20BA6922",
    "RAJ Transport",
    2500,
    195.84,
    ""
  ),
  createData(
    "Chennai DC",
    257973,
    "view_stores",
    2560,
    2560,
    2,
    279,
    "TN14M4795",
    "RAJ Transport",
    2500,
    204.972,
    ""
  ),
  createData(
    "Chennai DC",
    257974,
    "view_stores",
    4200,
    4200,
    1,
    283,
    "TN22DB3831",
    "RAJ Transport",
    5500,
    415.732,
    ""
  ),
];

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  {
    id: "FMS_Location",
    numeric: false,
    disablePadding: false,
    label: "FMS Location",
  },
  {
    id: "Temp_Trip_ID",
    numeric: true,
    disablePadding: false,
    label: "Trip ID",
  },
  {
    id: "Store_details",
    numeric: false,
    disablePadding: false,
    label: "Store details",
  },
  {
    id: "total_lpn",
    numeric: true,
    disablePadding: false,
    label: "total lpn Kgs",
  },
  {
    id: "total_lpn_vol_cft",
    numeric: false,
    disablePadding: false,
    label: "total lpn vol cft",
  },

  {
    id: "store_drops",
    numeric: true,
    disablePadding: false,
    label: "store drops",
  },
  {
    id: "distance_travelled",
    numeric: true,
    disablePadding: false,
    label: "distance_travelled",
  },
  {
    id: "vehicle_number",
    numeric: true,
    disablePadding: false,
    label: "vehicle_number",
  },

  {
    id: "vendor",
    numeric: false,
    disablePadding: false,
    label: "vendor",
  },
  {
    id: "vehicle_wt_capacity",
    numeric: true,
    disablePadding: false,
    label: "vehicle_wt_capacity",
  },
  {
    id: "vehicle_vol_capacity",
    numeric: true,
    disablePadding: false,
    label: "vehicle_vol_capacity",
  },
  { id: "vehicle_picture", numeric: false, disablePadding: false, label: "vehicle_picture" },
  
];

function EnhancedTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? "right" : "left"}
            padding={headCell.disablePadding ? "none" : "default"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(1),
  },
  table: {
    minWidth: 100,
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
}));

export default function EnhancedTablePermanent() {
  const classes = useStyles();
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("Temp_Trip_ID");

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {stableSort(rows, getComparator(order, orderBy)).map(
                (row, index) => {
                  const labelId = `FMS Location-${index}`;

                  return (
                    <TableRow hover key={row.FMS_Location}>
                      <TableCell id={labelId} scope="row" align="right">
                        {row.FMS_Location}
                      </TableCell>
                      <TableCell align="right">{row.Temp_Trip_ID}</TableCell>
                      <TableCell align="right">{row.Store_details}</TableCell>
                      <TableCell align="right">{row.total_lpn}</TableCell>
                      <TableCell align="right">{row.total_lpn_vol_cft}</TableCell>
                      <TableCell align="right">{row.store_drops}</TableCell>
                      <TableCell align="right">{row.distance_travelled}</TableCell>
                      <TableCell align="right">{row.vehicle_number}</TableCell>
                      <TableCell align="right">{row.vendor}</TableCell>
                      <TableCell align="right">
                        {row.vehicle_wt_capacity}
                      </TableCell>
                      <TableCell align="right">
                        {row.vehicle_vol_capacity}
                      </TableCell>
                      <TableCell align="right">
                      <Button variant="outlined" color="secondary">Upload</Button>
                        {row.vehicle_picture}
                      </TableCell>
                    </TableRow>
                  );
                }
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </div>
  );
}
