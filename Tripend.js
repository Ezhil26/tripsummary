import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import { TextField } from "@material-ui/core";
function createData(
  FMS_Location,
  Trip_ID,
  Store,
  Store_name,
  Vehicle_arrival_time,
  Vehicle_dispatch_time,
  POD
) {
  return {
    FMS_Location,
    Trip_ID,
    Store,
    Store_name,
    Vehicle_arrival_time,
    Vehicle_dispatch_time,
    POD,
  };
}

const rows = [
  createData("Chennai DC", 249986, 1182, "Thiruvanmiyur", "", "", ""),
  createData("Chennai DC", 249986, 1185, "Thorapakkam", "", "", ""),
  createData("Chennai DC", 257973, 1169, "Gowrivakkam", "", "", ""),
];

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  {
    id: "FMS_Location",
    numeric: false,
    disablePadding: false,
    label: "FMS Location",
  },
  {
    id: "Trip_ID",
    numeric: true,
    disablePadding: false,
    label: "Trip ID",
  },
  {
    id: "Store",
    numeric: true,
    disablePadding: false,
    label: "Store",
  },
  {
    id: "Store_name",
    numeric: true,
    disablePadding: false,
    label: "Store_name",
  },
  {
    id: "Vehicle_arrival_time",
    numeric: true,
    disablePadding: false,
    label: "Vehicle_arrival_time",
  },
  {
    id: "Vehicle_dispatch_time",
    numeric: true,
    disablePadding: false,
    label: "Vehicle_dispatch_time",
  },
  {
    id: "POD",
    numeric: true,
    disablePadding: false,
    label: "POD",
  },
];

function EnhancedTableHead(props) {
  const {
    classes,

    order,
    orderBy,

    onRequestSort,
  } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? "right" : "left"}
            padding={headCell.disablePadding ? "none" : "default"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
}));

export default function EnhancedTableTripend() {
  const classes = useStyles();
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("Temp_Trip_ID");

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {stableSort(rows, getComparator(order, orderBy)).map(
                (row, index) => {
                  const labelId = `FMS_Location-${index}`;

                  return (
                    <TableRow hover key={row.FMS_Location}>
                      <TableCell id={labelId} scope="row" align="right">
                        {row.FMS_Location}
                      </TableCell>
                      <TableCell align="right">{row.Trip_ID}</TableCell>
                      <TableCell align="right">{row.Store}</TableCell>
                      <TableCell align="right">{row.Store_name}</TableCell>
                      <TableCell align="right">
                        <TextField
                          id="datetime-local"
                          label="vehicle-arrival-time"
                          type="datetime-local"
                          defaultValue=""
                          className={classes.textField}
                          InputLabelProps={{
                            shrink: true,
                          }}
                        />
                        {row.Vehicle_arrival_time}
                      </TableCell>
                      <TableCell align="right">
                      <TextField
                          id="datetime-local"
                          label="vehicle-dispatch-time"
                          type="datetime-local"
                          defaultValue=""
                          className={classes.textField}
                          InputLabelProps={{
                            shrink: true,
                          }}
                        />
                        {row.Vehicle_dispatch_time}
                      </TableCell>
                      <TableCell align="right">
                        <Button variant="outlined" color="secondary">
                          POD
                        </Button>
                        {row.POD}
                      </TableCell>
                    </TableRow>
                  );
                }
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </div>
  );
}
