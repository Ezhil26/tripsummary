import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Paper from "@material-ui/core/Paper";


function createData(Temp_Trip_ID,Store_tag,Store_city_zone,Store_code,Store_name,total_lpn_wt,total_fv_wt,total_wt,total_lpn_vol_cft,total_fv_vol_cft,total_vol_demand,lpn_count,fv_crate_count,store_eta,store_end)
{
  return { Temp_Trip_ID,Store_tag,Store_city_zone,Store_code,Store_name,total_lpn_wt,total_fv_wt,total_wt,total_lpn_vol_cft,total_fv_vol_cft,total_vol_demand,lpn_count,fv_crate_count,store_eta,store_end}
}

const rows = [
  createData("T249986","Local","CHENNAI",1182,"Thiruvamniyur",258,412,670,388,234,622,13,10,"6:30AM","6:45AM"),
  createData("T249987","Local","CHENNAI",1185,"Mylapore",200,452,652,200,204,404,23,10,"8:00AM","8:15AM"),
  createData("T257974","Local","CHENNAI",1169,"Thorapakam",657,412,909,352,214,566,17,6,"6:00AM","6:15AM"),
  
];

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  {
    id: "Temp_Trip_ID",
    numeric: false,
    disablePadding: false,
    label: "Temp Trip ID",
  },
  { id: "Store_tag", numeric: false, disablePadding: false, label: "Store tag" },
  { id: "Store_city_zone", numeric: false, disablePadding: false, label: "Store city zone" },
  { id: "Store_code", numeric: true, disablePadding: false, label: "Store code" },
  { id: "Store_name", numeric: false, disablePadding: false, label: "Store name" },
  { id: "total_lpn_wt", numeric: true, disablePadding: false, label: "total_lpn_wt_Kgs" },
  { id: "total_fv_wt", numeric: true, disablePadding: false, label: "total_fv_wt_Kgs" },
  { id: "total_wt", numeric: true, disablePadding: false, label: "total_wt_demand" },
  { id: "total_lpn_vol_cft", numeric: true, disablePadding: false, label: "total_lpn_vol_cft" },
  { id: "total_fv_vol_cft", numeric: true, disablePadding: false, label: "total_fv_vol_cft" },
  { id: "total_vol_demand", numeric: true, disablePadding: false, label: "total_vol_demand" },
  { id: "lpn_count", numeric: true, disablePadding: false, label: "lpn_count" },
  { id: "fv_crate_count", numeric: true, disablePadding: false, label: "fv_crate_count" },
  { id: "store_eta", numeric: false, disablePadding: false, label: "store_eta" },
  { id: "store_end", numeric:false, disablePadding: false, label: "store_end" },
];

function EnhancedTableHead(props) {
  const {
    classes,
    order,
    orderBy,
    onRequestSort,
  } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? "right" : "left"}
            padding={headCell.disablePadding ? "none" : "default"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
};




const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(1),
  },
  table: {
    minWidth:100,
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
}));

export default function EnhancedTableStore() {
  const classes = useStyles();
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("Temp_Trip_ID");


  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };
 



  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {stableSort(rows, getComparator(order, orderBy))
                .map((row, index) => {
                  const labelId = `Temp_Trip_ID-${index}`;

                  return (
                    <TableRow
                    hover
                      
                      key={row.Temp_Trip_ID}
                      
                    >
                       <TableCell id={labelId}
                        scope="row" align="right">{row.Temp_Trip_ID}</TableCell>
                      <TableCell align="right">{row.Store_tag}</TableCell>
                      <TableCell align="right">{row.Store_city_zone}</TableCell>
                      <TableCell align="right">{row.Store_code}</TableCell>
                      <TableCell align="right">{row.Store_name}</TableCell>
                      <TableCell align="right">{row.total_lpn_wt}</TableCell>
                      <TableCell align="right">{row.total_fv_wt}</TableCell>
                      <TableCell align="right">{row.total_wt}</TableCell>
                      <TableCell align="right">{row.total_lpn_vol_cft}</TableCell>
                      <TableCell align="right">{row.total_fv_vol_cft}</TableCell>
                      <TableCell align="right">{row.total_vol_demand}</TableCell>
                      <TableCell align="right">{row.lpn_count}</TableCell>
                      <TableCell align="right">{row.fv_crate_count}</TableCell>
                      <TableCell align="right">{row.store_eta}</TableCell>
                      <TableCell align="right">{row.store_end}</TableCell>
                    </TableRow>
                  );
                })}
            
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </div>
  );
}
