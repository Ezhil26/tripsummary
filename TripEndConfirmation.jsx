
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import { TableHeader, StyledTableRow } from "../../Base/TableHeader";
import { getComparator, sortArray } from "../../../helpers/sorting";
import Typography from "@material-ui/core/Typography";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import Button from "@material-ui/core/Button";
import clsx from "clsx";

const headers = [
    {
      id: "FMS_Location",
      numeric: false,
      disablePadding: false,
      label: "FMS Location",
    },
    {
      id: "Trip_ID",
      numeric: true,
      disablePadding: false,
      label: "Trip ID",
    },
    {
      id: "Store",
      numeric: true,
      disablePadding: false,
      label: "Store",
    },
    {
      id: "Store_drops",
      numeric: true,
      disablePadding: false,
      label: "Store drops",
    },
    {
      id: "distance_travelled",
      numeric: true,
      disablePadding: false,
      label: "distance travelled",
    },
    {
      id: "vehicle_number",
      numeric: true,
      disablePadding: false,
      label: "vehicle number",
    },
    {
      id: "vendor",
      numeric: true,
      disablePadding: false,
      label: "vendor",
    },
    {
      id: "contract_type",
      numeric: true,
      disablePadding: false,
      label: "contract type",
    },
    {
      id: "Actual_distance",
      numeric: true,
      disablePadding: false,
      label: "Actual Distance (Km)",
    },
    {
      id: "Toll_charges",
      numeric: true,
      disablePadding: false,
      label: "Toll Charges",
    },
    {
      id: "Halting_charges",
      numeric: true,
      disablePadding: false,
      label: "Halting Charges",
    },
    {
      id: "other_charges",
      numeric: true,
      disablePadding: false,
      label: "Other Charges",
    },
    {
      id: "Total_other_charges",
      numeric: true,
      disablePadding: false,
      label: "Total Other Charges",
    },
  ];
  const useStyles = makeStyles((theme) => ({
    fs12: {
      fontSize: "12px",
    },
    dialogWidth: {
      minWidth: 800,
      maxWidth: 800,
    },
}));
export default function TripEndConfirmation(props) {
    const classes = useStyles();
    const [order, setOrder] = React.useState("asc");
    const [orderBy, setOrderBy] = React.useState("FMS_Location");
    const {tripData,handleNext}=props;
    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === "asc";
        setOrder(isAsc ? "desc" : "asc");
        setOrderBy(property);
      };
      return (
        <TableContainer className={classes.container}>
          <Table stickyHeader>
            <TableHeader
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={tripData.length}
              headers={headers}
              isCheckboxEnabled={false}
            />
            <TableBody>
              {sortArray(tripData, getComparator(order, orderBy)).map(
                (row, index) => {
                  return (
                    <StyledTableRow key={index}>
                      <TableCell align="right" className={classes.fs12}>
                        {row.FMS_Location}
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        {row.Trip_ID}
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        {row.Store}
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        {row.Store_drops}
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        {row.distance_travelled}
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        {row.vehicle_number}
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        {row.vendor}
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        {row.contract_type}
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        <Input
                          value={row.Actual_distance}
                          className={classes.fs12}
                          type="number"
                          
                        />
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        <Input
                          value={row.Toll_charges}
                          className={classes.fs12}
                          type="number"
                          
                          startAdornment={
                            <InputAdornment position="start">
                              <Typography className={classes.fs12}>
                                &#8377;
                              </Typography>
                            </InputAdornment>
                          }
                        />
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        <Input
                          value={row.Halting_charges}
                          className={classes.fs12}
                          type="number"
                          
                          startAdornment={
                            <InputAdornment position="start">
                              <Typography className={classes.fs12}>
                                &#8377;
                              </Typography>
                            </InputAdornment>
                          }
                        />
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        <Input
                          value={row.other_charges}
                          className={classes.fs12}
                          type="number"
                          
                          startAdornment={
                            <InputAdornment position="start">
                              <Typography className={classes.fs12}>
                                &#8377;
                              </Typography>
                            </InputAdornment>
                          }
                        />
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        {row.Total_other_charges}
                      </TableCell>
                      <TableCell align="right" className={classes.fs12}>
                        {row.Comments}
                      </TableCell>
                    </StyledTableRow>
                  );
                }
              )}
            </TableBody>
          </Table>
          <div className="flex justify-center pa3">
    <Button variant="contained" disableElevation onClick={handleNext}>
      <Typography className={clsx(classes.fs12, "ttn")}>
        Send to NTM for Approval
      </Typography>
    </Button>
  </div>
        </TableContainer>
      );
            }