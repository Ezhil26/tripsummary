import React from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Divider from "@material-ui/core/Divider";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import { QontoConnector, QontoStepIcon } from "../Base/StepperStyle";
import { moreColor } from "../../assets/css/BaseStyles";
import Button from "@material-ui/core/Button";
import ChevronLeftRoundedIcon from "@material-ui/icons/ChevronLeftRounded";
import GetAppRoundedIcon from "@material-ui/icons/GetAppRounded";
import PrintOutlinedIcon from "@material-ui/icons/PrintOutlined";
import TripEndConfirmation from "./TripEndConfirmation/TripEndConfirmation";
import Verification from "./Verification/Verification";
import Input from "@material-ui/core/Input";

const useStyles = makeStyles((theme) => ({
  fs12: {
    fontSize: "12px",
  },
  completedStepLabel: {
    color: `${moreColor[0]} !important`,
  },
  button: {
    marginRight: theme.spacing(1),
  },
}));

function createData(
  FMS_Location,
  Trip_ID,
  Store,
  Store_drops,
  distance_travelled,
  vehicle_number,
  vendor,
  contract_type,
  Actual_distance,
  Toll_charges,
  Halting_charges,
  other_charges,
  Total_other_charges,
  Comments
) {
  return {
    FMS_Location,
    Trip_ID,
    Store,
    Store_drops,
    distance_travelled,
    vehicle_number,
    vendor,
    contract_type,
    Actual_distance,
    Toll_charges,
    Halting_charges,
    other_charges,
    Total_other_charges,
    Comments,
  };
}
const rows = [
  createData(
    "Chennai DC",
    249986,
    "view_stores",
    2,
    253,
    "TN29BA6922",
    "RAJ TRANSPORT",
    "Fixed+var",
    "",
    "",
    "",
    "",
    "",
    ""
  ),
  createData(
    "Chennai DC",
    249986,
    "view_stores",
    2,
    310,
    "TN14M4795",
    "RAJ TRANSPORT",
    "Fixed+var",
    "",
    "",
    "",
    "",
    "",
    ""
  ),
  createData(
    "Chennai DC",
    257973,
    "view_stores",
    1,
    189,
    "TN22DB3831",
    "RAJ TRANSPORT",
    "Fixed+var with",
    "",
    "",
    "",
    "",
    "",
    ""
  ),
];

function getSteps() {
  return ["Verification", "Trip End Confirmation"];
}

function StepperHeader(props) {
  const classes = useStyles();
  const { activeStep, steps } = props;
  return (
    <Stepper activeStep={activeStep} connector={<QontoConnector />}>
      {steps.map((label) => (
        <Step key={label}>
          <StepLabel
            StepIconComponent={QontoStepIcon}
            classes={{
              completed: classes.completedStepLabel,
            }}
          >
            <Typography className={classes.fs12}>{label}</Typography>
          </StepLabel>
        </Step>
      ))}
    </Stepper>
  );
}

function StepperButton(props) {
  const classes = useStyles();
  const { handleBack, activeStep } = props;
  return (
    <React.Fragment>
      {activeStep > 0 ? (
        <Button
          onClick={handleBack}
          className={classes.button}
          variant="contained"
          disableElevation
          startIcon={<ChevronLeftRoundedIcon fontSize="small" />}
        >
          <Typography className={clsx(classes.fs12, "ttn")}>Back</Typography>
        </Button>
      ) : (
        ""
      )}
    </React.Fragment>
  );
}

export default function TripConfirmationStepper() {
  // Trip Creation Stepper
  const [activeStep, setActiveStep] = React.useState(0);
  const [tripData, setTripData] = React.useState(rows);
  const steps = getSteps();
  const handleTripData = (tripData) => {
    setTripData(tripData);
  };
  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const getStepContent = (step) => {
    switch (step) {
      case 0:
        return (
          <Verification
            activeStep={activeStep}
            tripData={tripData}
            handleTripData={handleTripData}
            handleNext={handleNext}
           
          />
        );
      case 1:
        return <TripEndConfirmation tripData={tripData} handleNext={handleNext}/>;
      default:
        return "";
    }
  };

  return (
    <div>
      <Paper elevation={0} square>
        <Grid container justify="space-between">
          <Grid item>
            <StepperHeader activeStep={activeStep} steps={steps} />
          </Grid>
          <Grid item className="self-center">
            <StepperButton handleBack={handleBack} activeStep={activeStep} />
          </Grid>
        </Grid>
      </Paper>
      <Paper elevation={0} square>
        <Divider />
        <React.Fragment>{getStepContent(activeStep)}</React.Fragment>
      </Paper>
    </div>
  );
}
